<?php
/**
 * @copyright Copyright (c) 2021 Thomas Citharel <nextcloud@tcit.fr>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\CSPEditor\Service;

use OCA\CSPEditor\AppInfo\Application;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\IConfig;

class CustomSecurityPolicyBuilder {
	/**
	 * @var IConfig
	 */
	private $config;

	public function __construct(IConfig $config) {
		$this->config = $config;
	}

	public function buildPolicy(): EmptyContentSecurityPolicy {
		$customCSP = $this->getJSONArrayConfigValue();

		$policy = new EmptyContentSecurityPolicy();
		$this->callAllowMethod($policy, $customCSP, 'allowInlineScript');
		$this->callAllowMethod($policy, $customCSP, 'allowEvalScript');
		$this->callAllowMethod($policy, $customCSP, 'allowInlineStyle');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedScriptDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedStyleDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFontDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedImageDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedConnectDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedMediaDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedObjectDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFrameDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFrameAncestorDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedWorkerSrcDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'allowedFormActionDomains');
		$this->callMethodOnDomains($policy, $customCSP, 'reportTo');

		return $policy;
	}

	/**
	 * @param EmptyContentSecurityPolicy $policy
	 * @param array $customCSP
	 * @param string $key
	 * @return void
	 * @psalm-param array<array-key, array<array-key, string>> $customCSP
	 */
	private function callMethodOnDomains(EmptyContentSecurityPolicy $policy, array $customCSP, string $key): void {
		if (isset($customCSP[$key])) {
			$method = $this->convertToMethod($key);
			foreach ($customCSP[$key] as $value) {
				$policy->$method($value);
			}
		}
	}

	private function callAllowMethod(EmptyContentSecurityPolicy $policy, array $customCSP, string $key): void {
		if (isset($customCSP[$key])) {
			$policy->$key($customCSP[$key]);
		}
	}

	private function convertToMethod(string $key): string {
		if ($key === 'reportTo') {
			return 'addReportTo';
		}
		return substr('add' . ucfirst($key), 0, -1);
	}

	/**
	 * @return array
	 * @psalm-return array<array-key,array<array-key, string>>
	 * @throws \JsonException
	 */
	private function getJSONArrayConfigValue(): array {
		$config = $this->config->getAppValue(Application::APP_NAME, 'customCSP', '[]');
		/**
		 * @var array $array
		 * @psalm-var array<array-key,array<array-key, string>>
		 */
		$array = json_decode($config, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
		return $array;
	}
}
