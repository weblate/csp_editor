# Changelog

## 1.0.0 - 2021-12-07

### Added

- Support for Nextcloud 22, 23 and 24.

### Changed

- Disabled allowing inline and eval scripts as it doesn't work because [of a core Nextcloud issue](https://github.com/nextcloud/server/issues/25226).

### Removed

- Support for Nextcloud 19, 20 and 21.


## 0.0.1 - 2020-01-08

Initial release
