<?php

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Service;

use OC\Security\CSP\ContentSecurityPolicyManager;
use OCA\CSPEditor\Event\CustomSecurityPolicyListener;
use OCA\CSPEditor\Service\CustomSecurityPolicyBuilder;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\Security\CSP\AddContentSecurityPolicyEvent;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class CustomSecurityPolicyListenerTest extends TestCase {
	/**
	 * @var CustomSecurityPolicyListener
	 */
	private $customSecurityPolicyListener;

	/**
	 * @var CustomSecurityPolicyBuilder|MockObject
	 */
	private $customSecurityPolicyBuilder;

	/**
	 * @var ContentSecurityPolicyManager
	 */
	private $contentSecurityPolicyManager;

	public function setUp(): void {
		$this->customSecurityPolicyBuilder = $this->createMock(CustomSecurityPolicyBuilder::class);
		$dispatcher = \OC::$server->get(IEventDispatcher::class);
		$this->contentSecurityPolicyManager = new ContentSecurityPolicyManager($dispatcher);
		$this->customSecurityPolicyListener = new CustomSecurityPolicyListener($this->customSecurityPolicyBuilder);
	}

	public function testWithBadEvent(): void {
		$event = new Event();
		$this->customSecurityPolicyBuilder->expects($this->never())->method('buildPolicy');
		$this->customSecurityPolicyListener->handle($event);
	}

	public function testAddCustomPolicy(): void {
		$policy = new EmptyContentSecurityPolicy();
		$policy->allowEvalScript(true);
		$event = $this->createMock(AddContentSecurityPolicyEvent::class);
		$this->customSecurityPolicyBuilder->expects($this->once())->method('buildPolicy')->willReturn($policy);
		$event->expects($this->once())->method('addPolicy')->with($policy);
		$this->customSecurityPolicyListener->handle($event);
	}
}
