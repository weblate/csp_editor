<?php

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Service;

use OCA\CSPEditor\AppInfo\Application;
use OCA\CSPEditor\Service\CustomSecurityPolicyBuilder;
use OCP\AppFramework\Http\EmptyContentSecurityPolicy;
use OCP\IConfig;
use PHPUnit\Framework\MockObject\MockObject;
use ChristophWurst\Nextcloud\Testing\TestCase;

class CustomSecurityPolicyBuilderTest extends TestCase {

	/**
	 * @var IConfig|MockObject
	 */
	private $config;
	/**
	 * @var CustomSecurityPolicyBuilder
	 */
	private $customSecurityPolicyBuilder;

	public function setUp(): void {
		$this->config = $this->createMock(IConfig::class);
		$this->customSecurityPolicyBuilder = new CustomSecurityPolicyBuilder($this->config);
	}

	public function testBuildCustomPolicy(): void {
		$this->config->expects($this->once())->method('getAppValue')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn('{"allowInlineScript":true, "allowedFontDomains": ["something.com", "like.com"], "reportTo": ["someone@somewhere.org"]}');

		$expectedPolicy = new EmptyContentSecurityPolicy();
		$expectedPolicy->allowInlineScript(true);
		$expectedPolicy->addAllowedFontDomain('something.com');
		$expectedPolicy->addAllowedFontDomain('like.com');
		$expectedPolicy->addReportTo("someone@somewhere.org");

		$this->assertEquals($expectedPolicy, $this->customSecurityPolicyBuilder->buildPolicy());
	}

	public function testBuildCustomPolicyWithEmptyArray(): void {
		$this->config->expects($this->once())->method('getAppValue')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn('{}');

		$expectedPolicy = new EmptyContentSecurityPolicy();

		$this->assertEquals($expectedPolicy, $this->customSecurityPolicyBuilder->buildPolicy());
	}
}
