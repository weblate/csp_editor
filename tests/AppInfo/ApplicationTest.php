<?php

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\BackgroundJob;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\AppInfo\Application;

class ApplicationTest extends TestCase {
	/**
	 * @var Application
	 */
	private $application;

	public function setUp(): void {
		parent::setUp();
		$this->application = new Application();
	}

	public function testApplicationName(): void {
		$this->assertEquals(Application::APP_NAME, $this->application->getContainer()->getAppName());
	}
}
