<?php

declare(strict_types=1);

namespace OCA\CSPEditor\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\CSPEditor\AppInfo\Application;
use OCA\CSPEditor\Settings\Admin;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IConfig;
use PHPUnit\Framework\MockObject\MockObject;

class AdminTest extends TestCase {
	/**
	 * @var Admin
	 */
	private $admin;
	/**
	 * @var IInitialState|MockObject
	 */
	private $initialState;
	/**
	 * @var IConfig|MockObject
	 */
	private $config;

	public function setUp(): void {
		parent::setUp();
		$this->config = $this->createMock(IConfig::class);
		$this->initialState = $this->createMock(IInitialState::class);

		$this->admin = new Admin($this->config, $this->initialState);
	}

	public function testGetPriority(): void {
		$this->assertEquals(80, $this->admin->getPriority());
	}

	public function testGetSection(): void {
		$this->assertEquals('additional', $this->admin->getSection());
	}

	/**
	 * @param string $cspString
	 * @param bool $debug
	 * @dataProvider dataForTestGetForm
	 */
	public function testGetForm(string $cspString, bool $debug): void {
		$res = new TemplateResponse(Application::APP_NAME, 'admin');

		$this->config->expects($this->once())->method('getAppValue')->with(Application::APP_NAME, 'customCSP', '[]')->willReturn($cspString);
		$this->config->expects($this->once())->method('getSystemValueBool')->with('debug', false)->willReturn($debug);

		$this->initialState->expects($this->exactly(2))->method('provideInitialState')->withConsecutive(
			['customCSP', $cspString],
			['debug', $debug]
		);

		$this->assertEquals($res, $this->admin->getForm());
	}

	public function dataForTestGetForm(): array {
		return [
			[
				'[]', false,
				'[]', true
			]
		];
	}
}
