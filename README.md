# CSP Editor

**An app to edit [Nextcloud](https://nextcloud.com)'s default CSP policy**

## Installation

In your Nextcloud, simply navigate to *Apps*, choose the category *Tools*, find the **CSP Editor** app and enable it.

You can also `git clone` or download this repository and put it in your `apps` folder, run `npm i` and `composer i` to install dependencies, `npm run build` to build front-end, then finally enable the app inside the *Apps* section in Nextcloud.

## Support

Please open issues here : https://framagit.org/framasoft/nextcloud/csp_editor/issues (English prefered, French accepted)

## Maintainers:

- [tcitworld](https://framagit.org/tcit)

If you'd like to join, just go through the [issue list](https://framagit.org/framasoft/nextcloud/csp_editor/issues) and fix some. :)

